#include "Condition.h"

bool is_float(const std::string &str) {
    if(str[0] != '-' && !isdigit(str[0]))
        return false;
	unsigned dot = 0;
        std::string sub(str.begin()+1, str.end());
    for (const auto &ch : sub) {
		if (!isdigit(ch) && ch != '.')
			return false;
		else if (ch == '.')
			++dot;
	}
	if (dot > 1)
		return false;
	return true;
}



bool Condition::evaluate(std::vector<VariableContainer> &var_container) {
	if (condition.size() < 1 || condition.size() > 3)
		throw std::logic_error("Wrong condition");
	if (condition.size() == 1) {
		if (condition[0].get_type() == Token::Type::constant) {
			if (!is_float(condition[0].token()))
				throw std::logic_error("Wrong condition. Expected int or float");
			return std::stof(condition[0].token());
		}
		else if (condition[0].get_type() == Token::Type::varname) {
			for (int i = var_container.size() - 1; i >= 0; --i)
			{
				if (var_container[i].exist(condition[0].token())) {
					if (!is_float(var_container[i][condition[0].token()]))
						throw std::logic_error("Wrong condition. Expected int or float");

					return std::stof(var_container[i][condition[0].token()]);
				}
			}
			throw std::logic_error("Wrong condition. Variable doesn't exist!");
		}
		else
			throw std::logic_error("Wrong condition. Expected constant or variable!");
	}


	else if (condition.size() == 2) {
		if (condition[0].token() != "!")
			throw std::logic_error("Unary '!' expected!");
		if (condition[1].get_type() == Token::Type::constant) {
			if (!is_float(condition[1].token()))
				throw std::logic_error("Wrong condition. Expected int or float");
			return !std::stof(condition[1].token());
		}
		else if (condition[1].get_type() == Token::Type::varname) {
			for (int i = var_container.size() - 1; i >= 0; --i)
			{
				if (var_container[i].exist(condition[1].token())) {
					if (!is_float(var_container[i][condition[1].token()]))
						throw std::logic_error("Wrong condition. Expected int or float");

					return !std::stof(var_container[i][condition[1].token()]);
				}
			}
			throw std::logic_error("Wrong condition. Variable doesn't exist!");
		}
		else
			throw std::logic_error("Wrong condition. Expected constant or variable!");
	}


	else if (condition.size() == 3) {
		if (condition[1].get_type() != Token::Type::logical_oper)
			throw std::logic_error("Wrong operator in condition!");

		float temp1;
		float temp2;

		if ((condition[0].get_type() != Token::Type::constant && condition[0].get_type() != Token::Type::varname)
			|| (condition[0].get_type() != Token::Type::constant && condition[0].get_type() != Token::Type::varname))
			throw std::logic_error("Wrong values in condition!");
		if (condition[0].get_type() == Token::Type::varname)
		{
			bool is_exist = false;
			for (int i = var_container.size() - 1; i >= 0; --i)
			{
				if (var_container[i].exist(condition[0].token())) {
					is_exist = true;
					if (!is_float(var_container[i][condition[0].token()]))
						throw std::logic_error("Wrong condition. Expected int or float");

					temp1 = std::stof(var_container[i][condition[0].token()]);
					break;
				}
			}
			if(!is_exist)
				throw std::logic_error("Wrong condition. Variable doesn't exist!");
		}
		else {
			if (!is_float(condition[0].token()))
				throw std::logic_error("Wrong condition. Expected int or float");
			temp1 = std::stof(condition[0].token());
		}

		if (condition[2].get_type() == Token::Type::varname)
		{
			bool is_exist = false;
			for (int i = var_container.size() - 1; i >= 0; --i)
			{
				if (var_container[i].exist(condition[2].token())) {
					is_exist = true;
					if (!is_float(var_container[i][condition[2].token()]))
						throw std::logic_error("Wrong condition. Expected int or float");

					temp2 = std::stof(var_container[i][condition[2].token()]);
					break;
				}
			}
			if (!is_exist)
				throw std::logic_error("Wrong condition. Variable doesn't exist!");
		}
		else {
			if (!is_float(condition[2].token()))
				throw std::logic_error("Wrong condition. Expected int or float");
			temp2 = std::stof(condition[2].token());
		}

		if (condition[1].token() == "<")
			return temp1 < temp2;
		else if (condition[1].token() == "<=")
			return temp1 <= temp2;
		else if (condition[1].token() == ">")
			return temp1 > temp2;
		else if (condition[1].token() == ">=")
			return temp1 >= temp2;
		else if (condition[1].token() == "==")
			return temp1 == temp2;
		else if (condition[1].token() == "!=")
			return temp1 != temp2;
		else throw std::logic_error("Wrong condition logical operator");


	}

}

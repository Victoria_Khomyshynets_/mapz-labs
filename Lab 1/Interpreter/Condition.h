#ifndef CONDITION_H
#define CONDITION_H

#include <iostream>
#include <vector>
#include <string>
#include "Token.h"
#include "VarContainer.h"




class Condition {
private:
	std::vector<Token> condition;

public:
	Condition(const std::vector<Token> &cond) : condition(cond) {}

	bool evaluate(std::vector<VariableContainer> &var_container);

};






#endif // !CONDITION_H
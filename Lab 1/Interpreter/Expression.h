#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <stack>
#include <vector>
#include <string>
#include <sstream>
#include "Interpreter.h"

#include "Token.h"

class Expression
{
	std::vector<Token> expr_;

public:
	explicit Expression(const std::vector<Token> &expr) : expr_{ std::move(expr) } {}
	bool is_correct();
	std::string evaluate(std::vector<VariableContainer> &var_container);
	bool is_float(const std::string &str);
private:
	static int prec(const Token &op_token);
	static std::vector<Token> infix_to_postfix(const std::vector<Token> &expr, std::vector<VariableContainer> &var_container);
};

bool Expression::is_correct()
{
	std::vector<Token> stack;
	for (auto tkn : expr_)
	{
		if (tkn.get_type() == Token::Type::lparent)
		{
			stack.push_back(tkn);
		}
		else if (tkn.get_type() == Token::Type::rparent)
		{
			if (stack.empty() || stack.back().get_type() != Token::Type::rparent)
				return false;
			stack.pop_back();
		}
	}
	for (auto i = 0; i < expr_.size() - 1; ++i)
	{
		const auto nextToken = expr_[i + 1];
		const auto currentToken = expr_[i];

		const bool isNextTokenBinOp = nextToken.token() == "+" || nextToken.token() == "-"
			|| nextToken.token() == "/" || nextToken.token() == "*";

		const bool isCurrentTokenBinOp = currentToken.token() == "+" || currentToken.token() == "-"
			|| currentToken.token() == "/" || currentToken.token() == "*";

		if (currentToken.get_type() == Token::Type::constant && (!isNextTokenBinOp && nextToken.get_type() != Token::Type::rparent))
		{
			return false;
		}
		if (currentToken.get_type() == Token::Type::rparent && !isNextTokenBinOp)
		{
			return false;
		}
		if (isCurrentTokenBinOp && isNextTokenBinOp)
		{
			return false;
		}
		if (currentToken.get_type() == Token::Type::constant && nextToken.get_type() == Token::Type::constant)
		{
			return false;
		}
	}
	return true;
}


bool Expression::is_float(const std::string &str) {
    if(str[0] != '-' && !isdigit(str[0]))
        return false;
	unsigned dot = 0;
    std::string sub(str.begin()+1, str.end());
    for (const auto &ch : sub) {
		if (!isdigit(ch) && ch != '.')
			return false;
		else if (ch == '.')
			++dot;
	}
	if (dot > 1)
		return false;
	return true;
}

std::string Expression::evaluate(std::vector<VariableContainer> &var_container)
{
	expr_ = Expression::infix_to_postfix(expr_, var_container);
	std::stack<std::string> stack;
	for (auto tkn : expr_)
	{
		if (tkn.get_type() == Token::Type::constant)
		{
			stack.push(tkn.token());
		}
		else if (tkn.get_type() == Token::Type::varname)
		{
			for (int i = var_container.size() - 1; i >= 0; --i)
			{
				if (var_container[i].exist(tkn.token())) {
					stack.push(var_container[i][tkn.token()]);
					break;
				}
			}
		}
		else
		{			
            if(stack.size()<2)
                throw std::logic_error("Wrong expression!");
			const auto leftValue = stack.top();
			stack.pop();
			const auto rightValue = stack.top();
			stack.pop();

			std::string temp;
			if (tkn.token() == "+")
			{
				if (is_float(rightValue) && is_float(leftValue))
					temp = std::to_string(stof(rightValue) + stof(leftValue));
				else
					temp = rightValue + leftValue;
				stack.push(temp);
			}
			else if (tkn.token() == "-")
			{
				if (is_float(rightValue) && is_float(leftValue))
					temp = std::to_string(stof(rightValue) - stof(leftValue));
				else
                    throw std::logic_error("Wrong operation on string!");
				stack.push(temp);
			}
			else if (tkn.token() == "/")
			{
				if (is_float(rightValue) && is_float(leftValue)) {
					if (stof(leftValue) == 0)
						throw std::logic_error("Division by zero!");
					temp = std::to_string(stof(rightValue) / stof(leftValue));
				}
				else
                    throw std::logic_error("Wrong operation on string!");
				stack.push(temp);
			}
			else if (tkn.token() == "*")
			{
				if (is_float(rightValue) && is_float(leftValue))
					temp = std::to_string(stof(rightValue) * stof(leftValue));
				else
                    throw std::logic_error("Wrong operation on string!");
				stack.push(temp);
			}
			else
                throw std::logic_error("Unknown operator in expression!");
		}
	}
	return stack.top();
}

int Expression::prec(const Token& op_token)
{
	if (op_token.token() == "*" || op_token.token() == "/")
	{
		return 2;
	}
	if (op_token.token() == "+" || op_token.token() == "-")
		return 1;
	return -1;
}

std::vector<Token> Expression::infix_to_postfix(const std::vector<Token>& expr, std::vector<VariableContainer> &var_container)
{
	std::vector<Token> result;
	std::stack<Token> stack;
	for (auto tkn : expr)
	{
		const auto tokenType = tkn.get_type();
		if (tokenType == Token::Type::constant)
			result.push_back(tkn);
		else if (tokenType == Token::Type::varname)
		{
			bool exists = false;
			for (int i = var_container.size() - 1; i >= 0; --i)
			{
				if (var_container[i].exist(tkn.token())) {
					exists = true;
					break;
				}
			}
			if(!exists)	throw std::logic_error("This variable doesn`t exist!");
			result.push_back(tkn);
		}
		else if (tokenType == Token::Type::lparent)
			stack.push(tkn);
		else if (tokenType == Token::Type::rparent)
		{
			while (!stack.empty() && stack.top().get_type() != Token::Type::lparent)
			{
				result.push_back(stack.top());
				stack.pop();
			}
			if (!stack.empty() && stack.top().get_type() != Token::Type::lparent)
			{
				throw std::exception();
			}
			stack.pop();
		}
		else
		{
			while (!stack.empty() && prec(tkn) <= prec(stack.top()))
			{
				result.push_back(stack.top());
				stack.pop();
			}
			stack.push(tkn);
		}
	}
	while (!stack.empty())
	{
		result.push_back(stack.top());
		stack.pop();
	}
	return result;
}

#endif

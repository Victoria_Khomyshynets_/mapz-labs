#include "Interpreter.h"

#include "Condition.h"

void Interpreter::execute()
{
	Preprocessor preproc{ code };
	code = preproc.execute();

	LexicalAnalyzer la{ code };
	std::vector<Token> tokens{ la.execute() };

	std::vector<std::unique_ptr<Statement>> statements;

	SyntaxAnalyzer sa{ tokens };
	sa.execute(var_container, statements);

	for (auto &x : statements)
		x->execute(var_container);


}

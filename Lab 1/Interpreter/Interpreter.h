#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <iostream>
#include <vector>
#include "VarContainer.h"
#include "Preprocessor.h"
#include "LexAnalyzer.h"
#include "SyntaxAnalyzer.h"



class Interpreter {
private:
	std::string code;
	std::vector<VariableContainer> var_container;

public:
    Interpreter() {var_container.push_back(VariableContainer());}
	Interpreter(std::string text) : code(text) { var_container.push_back(VariableContainer()); }
	void execute();
    void set_code(const std::string &str) { code = str; }



};






#endif

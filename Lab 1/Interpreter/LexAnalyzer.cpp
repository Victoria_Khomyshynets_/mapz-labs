#include "LexAnalyzer.h"
#include <string>


std::vector<Token> LexicalAnalyzer::execute()
{
	tokenize();
	return token_list;
}

bool iskeyword(std::string token) {
    std::vector<std::string> keywords = { "for", "while", "if", "elseif", "else", "find", "in", "print" };
	bool iskw = false;
	for (const auto &str : keywords)
	{
		if (!token.compare(str))
		{
			iskw = true;
			break;
		}
	}
	return iskw;
}

bool isvartype(std::string token) {
	std::vector<std::string> vartypes = { "int", "string" };
	bool isvt = false;
	for (const auto &str : vartypes)
	{
		if (!token.compare(str))
		{
			isvt = true;
			break;
		}
	}
	return isvt;
}

void LexicalAnalyzer::tokenize()
{

	for (unsigned i = 0; i < text.size();)
	{
		const char ch = text[i];

		if (isspace(ch))
		{
			++i;
		}
		else if (ch == '=' || ch == '>' || ch == '<' || ch == '!')
		{
			if (text[i + 1] == '=') {
				Token tok (std::string{ text.begin() + i, text.begin() + i + 2 }, Token::Type::logical_oper);
				token_list.push_back(tok);
				++i;
			} 
			else if (ch == '=') {
				Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::oper);
				token_list.push_back(tok);
			} else {
				Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::logical_oper);
				token_list.push_back(tok);
			}
			++i;
		} 
		else if (ch == '+')
		{
			if (text[i + 1] == '+') {
				Token tok(std::string{ text.begin() + i, text.begin() + i + 2 }, Token::Type::oper);
				token_list.push_back(tok);
				++i;
			} else {
				Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::oper);
				token_list.push_back(tok);
			}
			++i;
		}
		else if (ch == '-')
		{
			if (text[i + 1] == '-') {
				Token tok(std::string{ text.begin() + i, text.begin() + i + 2 }, Token::Type::oper);
				token_list.push_back(tok);
				++i;
            }
            else if(isdigit(text[i+1])) {
                unsigned j{ 1 };
                unsigned dot_counter = 0;
                while (isdigit(text[i + j]) || text[i + j]=='.') {
                    ++j;
                    if (text[i + j] == '.')
                        ++dot_counter;
                }
                if (dot_counter > 1)
                    throw std::logic_error("Floating point value can consist only of 1 dot!");
                std::string temp_token{ text.begin() + i, text.begin() + i + j };
                Token tok(temp_token, Token::Type::constant);
                token_list.push_back(tok);
                i += (j-1);
            }
            else {
				Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::oper);
				token_list.push_back(tok);
			}
			++i;
		}
		else if (ch == '*')
		{
			Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::oper);
			token_list.push_back(tok);
			++i;
		}
		else if (ch == '/')
		{
			Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::oper);
			token_list.push_back(tok);
			++i;
		}
		else if (ch == ';')
		{
			Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::semicolon);
			token_list.push_back(tok);
			++i;
		}
		else if (ch == ':')
		{
			Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::colon);
			token_list.push_back(tok);
			++i;
		}
		else if (ch == '(')
		{
			Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::lparent);
			token_list.push_back(tok);
			++i;
		}
		else if (ch == ')')
		{
			Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::rparent);
			token_list.push_back(tok);
			++i;
		}
		else if (ch == '{')
		{
			Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::lbrace);
			token_list.push_back(tok);
			++i;
		}
		else if (ch == '}')
		{
			Token tok(std::string{ text.begin() + i, text.begin() + i + 1 }, Token::Type::rbrace);
			token_list.push_back(tok);
			++i;
		}
		else if (isalpha(ch))
		{
			unsigned j{ 1 };
			while (isalnum(text[i + j]))
				++j;
			std::string temp_token{ text.begin()+i, text.begin()+i+j };
			Token::Type type = Token::Type::varname;
			if (iskeyword(temp_token))
				type = Token::Type::keyword;
			else if (isvartype(temp_token))
				type = Token::Type::vartype;
			Token tok(temp_token, type);
			token_list.push_back(tok);
			i += j;
		}
		else if (isdigit(ch))
		{
			unsigned j{ 1 };
			unsigned dot_counter = 0;
			while (isdigit(text[i + j]) || text[i + j]=='.') {
				++j;
				if (text[i + j] == '.')
					++dot_counter;
			}
			if (dot_counter > 1)
				throw std::logic_error("Floating point value can consist only of 1 dot!");
			std::string temp_token{ text.begin() + i, text.begin() + i + j };
			Token tok(temp_token, Token::Type::constant);
			token_list.push_back(tok);
			i += j;
		}
		else if (ch == '\"')
		{
			unsigned j{ 1 };
			while (text[i + j] != '\"')
				j++;
			std::string temp_token{ text.begin() + i+1, text.begin() + i + j };
			Token tok(temp_token, Token::Type::constant);
			token_list.push_back(tok);
			i += j+1;
		}
		else
			++i;
	}
}


#ifndef LEXANALYZER_H
#define LEXANALYZER_H

#include <iostream>
#include <vector>
#include <list>
#include "Token.h"


class LexicalAnalyzer {
private:
	std::vector<Token> token_list;
	std::string text;

	void tokenize();

public:
	LexicalAnalyzer(std::string txt) : text(txt) {}
	std::vector<Token> execute();


};




#endif
#include "preprocessor.h"


std::string Preprocessor::execute()
{
	clear_comments();
	replace_all("if(", "if (");
	replace_all("while(", "while (");
	return text;
}

void Preprocessor::clear_comments()
{
	size_t index_start = 0, index_end = 0;
	while (index_start != std::string::npos && index_end != std::string::npos)
	{

		index_start = text.find("/*", 0);
		index_end = text.find("*/", index_start + 2);

		if (index_end < text.length() && index_start < index_end)
			text.erase(text.begin() + index_start, text.begin() + index_end + 2);
	}
}


void Preprocessor::replace_all(const std::string pattern, const std::string replace)
{
	std::string::size_type n = 0;
	while ((n = text.find(pattern, n)) != std::string::npos)
	{
		text.replace(n, pattern.size(), replace);
		n += replace.size();
	}
}
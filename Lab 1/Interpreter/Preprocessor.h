#pragma once
#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H
#include <iostream>

class Preprocessor {
private:
	std::string text;

	void clear_comments();
	void replace_all(const std::string, const std::string);

public:
	Preprocessor(std::string txt) : text(txt) {}
	std::string execute();
};


#endif
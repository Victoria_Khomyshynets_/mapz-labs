#include <iostream>
#include <regex>
#include <thread>
#include <future>
#include <sstream>
#include <fstream>
#include <functional>
#include "Statement.h"
#include "SyntaxAnalyzer.h"
#include "Expression.h"

bool is_int(const std::string &str) {
    if(str[0] != '-' && !isdigit(str[0]))
        return false;
    std::string sub(str.begin()+1, str.end());
    for (const auto &ch : sub) {
		if (!isdigit(ch))
			return false;
	}
	return true;
}

void Assign::execute(std::vector<VariableContainer> &var_container)
{
	if (expression[0].get_type() != Token::Type::varname)
		throw std::logic_error("Wrong first token in assignment!");
	else if(expression[1].token() != "=")
		throw std::logic_error("Variable name must be followed by '='");
	else if (expression[2].token() == ";")
		throw std::logic_error("After '=' there must be an expression!");
	unsigned i{ 2 };
	while(expression[i].get_type() != Token::Type::semicolon)
	{
		if (expression[i].get_type() != Token::Type::constant
			&& expression[i].get_type() != Token::Type::varname
			&& expression[i].get_type() != Token::Type::oper
			&& expression[i].get_type() != Token::Type::lparent
			&& expression[i].get_type() != Token::Type::rparent)
		{
			throw std::logic_error("Invalid token in assignment!");
		}
		++i;
	}
	Expression exp(std::vector<Token>{ expression.begin() + 2, expression.begin() + i });
	std::string result = exp.evaluate(var_container);

	bool exists = false;
	for (int i = var_container.size() - 1; i >= 0; --i)
	{
		if (var_container[i].exist(expression[0].token())) {
			var_container[i][expression[0].token()] = result;
			exists = true;
		}
	}
	if (!exists)
		var_container[var_container.size() - 1][expression[0].token()] = result;
}


void IfBranch::execute(std::vector<VariableContainer> &var_container)
{
	if (condition.evaluate(var_container)) {
		var_container.push_back(VariableContainer());
		for (auto &x : if_body)
			x->execute(var_container);
		var_container.pop_back();
	}
	else {
		var_container.push_back(VariableContainer());
		for (auto &x : else_body)
			x->execute(var_container);
		var_container.pop_back();
	}
}


void WhileLoop::execute(std::vector<VariableContainer>& var_container)
{
	while (condition.evaluate(var_container)) {
		var_container.push_back(VariableContainer());
		for (auto &x : while_body)
			x->execute(var_container);

		var_container.pop_back();
	}

}

void ForLoop::execute(std::vector<VariableContainer>& var_container)
{
	if (condition[0].get_type() != Token::Type::varname || condition[1].token() != "=" || condition[3].get_type() != Token::Type::colon)
		throw std::logic_error("Wrong for condition");
	if (!is_int(condition[2].token()) || !is_int(condition[4].token()))
		throw std::logic_error("Wrong constants in for condition");

	int start = std::stoi(condition[2].token());
	int end = std::stoi(condition[4].token());

	var_container.push_back(VariableContainer());


	for (int i = start; i <= end; ++i) {
		var_container[var_container.size() - 1][condition[0].token()] = std::to_string(i);

		var_container.push_back(VariableContainer());
		for (auto &x : for_body)
			x->execute(var_container);

		var_container.pop_back();

		if (!is_int(var_container[var_container.size() - 1][condition[0].token()]))
			throw std::logic_error("Iterator can be only integer!");
		i = std::stod(var_container[var_container.size() - 1][condition[0].token()]);
	}
	var_container.pop_back();
}

void find(const std::string expr, std::string str, std::string &result)
{
	std::regex expression(expr);
	std::smatch matches;

	std::ostringstream res;

    res << "Target sequence: " << str << "\n";
	res << "Regular expression: " << expr << "\n";
	res << "The following matches and submatches were found:" << "\n";

    int position = 0;

	while (std::regex_search(str, matches, expression)) {
        for (auto x : matches) {

            res << x << " ";
            res << "\nIndex of match:   " << (position+matches.position()) << "\n";
            position += (matches.position()+x.length());
        }
		str = matches.suffix().str();
	}
	result = res.str();
}


void Find::execute(std::vector<VariableContainer> &var_container) 
{
	if (statement[0].token() != "find" || statement[2].token() != "in")
		throw std::logic_error("Invalid find statement!");
	if((statement[1].get_type() != Token::Type::constant && statement[1].get_type() != Token::Type::varname)
		|| (statement[3].get_type() != Token::Type::constant && statement[3].get_type() != Token::Type::varname))
		throw std::logic_error("Invalid find statement arguments!");

	std::string expr;
	std::string string;

	if (statement[1].get_type() == Token::Type::varname)
	{
		bool is_exist = false;
		for (int i = var_container.size() - 1; i >= 0; --i)
		{
			if (var_container[i].exist(statement[1].token())) {
				is_exist = true;

				expr = var_container[i][statement[1].token()];
				break;
			}
		}
		if (!is_exist)
			throw std::logic_error("Wrong find statement. Variable doesn't exist!");
	}
	else {
		expr = statement[1].token();
	}

	if (statement[3].get_type() == Token::Type::varname)
	{
		bool is_exist = false;
		for (int i = var_container.size() - 1; i >= 0; --i)
		{
			if (var_container[i].exist(statement[3].token())) {
				is_exist = true;

				string = var_container[i][statement[3].token()];
				break;
			}
		}
		if (!is_exist)
			throw std::logic_error("Wrong find statement. Variable doesn't exist!");
	}
	else {
		string = statement[3].token();
	}
	std::string result;
    std::thread find_thread(find, expr, string, std::ref(result));

    find_thread.join();
	// ТУТ ШЛЯХ ДО ФАЙЛУ
    std::ofstream file("D:\\Interpreter\\output.txt", std::ios::app | std::ios::out);
    file<<result;
    file.close();
    //std::cout << result;
}


void Print::execute(std::vector<VariableContainer> &var_container)
{
    if (expression[0].token() != "print")
        throw std::logic_error("Invalid print statement!");
    else if (expression[1].token() == ";")
        throw std::logic_error("After 'print' there must be an expression!");
    unsigned i{ 1 };
    while(expression[i].get_type() != Token::Type::semicolon)
    {
        if (expression[i].get_type() != Token::Type::constant
            && expression[i].get_type() != Token::Type::varname
            && expression[i].get_type() != Token::Type::oper
            && expression[i].get_type() != Token::Type::lparent
            && expression[i].get_type() != Token::Type::rparent)
        {
            throw std::logic_error("Invalid token in assignment!");
        }
        ++i;
    }
    Expression exp(std::vector<Token>{ expression.begin() + 1, expression.begin() + i });
    std::string result = exp.evaluate(var_container);
    result.push_back('\n');


    std::ofstream file("D:\\Interpreter\\output.txt", std::ios::app | std::ios::out);
    file<<result;
    file.close();
}

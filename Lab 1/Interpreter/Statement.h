#ifndef STATEMENT_H
#define STATEMENT_H


#include <vector>
#include <memory>
#include "Token.h"
#include "VarContainer.h"
#include "Condition.h"
//#include "Interpreter.h"


class Statement {
private:

public:
	virtual void execute(std::vector<VariableContainer> &var_container) = 0;
	virtual ~Statement() {}
	Statement() {}
	Statement(const std::vector<Token> tokens) {}
};

class Assign : public Statement {
private:
	std::vector<Token> expression;


public:
	Assign(const std::vector<Token> &tokens) : expression(tokens) {}
	void execute(std::vector<VariableContainer> &var_container) override;

};

class IfBranch : public Statement {
private:
	std::vector<std::unique_ptr<Statement>> if_body;
	std::vector<std::unique_ptr<Statement>> else_body;
	Condition condition;

public:
    IfBranch(const std::vector<Token> &cond, std::vector<std::unique_ptr<Statement>> &i_b, std::vector<std::unique_ptr<Statement>> &e_b)
		: condition(cond) {
		for (auto &x : i_b)
			if_body.emplace_back(x.release());
		for (auto &x : e_b)
			else_body.emplace_back(x.release());
	}

	void execute(std::vector<VariableContainer> &var_container) override;
};

class WhileLoop : public Statement {
private:
	std::vector<std::unique_ptr<Statement>> while_body;
	Condition condition;


public:
    WhileLoop(const std::vector<Token> &cond, std::vector<std::unique_ptr<Statement>> &w_b) : condition(cond) {
		for (auto &x : w_b)
			while_body.emplace_back(x.release());
	}

	void execute(std::vector<VariableContainer> &var_container) override;

};


class ForLoop : public Statement {
private:
	std::vector<std::unique_ptr<Statement>> for_body;
	std::vector<Token> condition;


public:
    ForLoop(const std::vector<Token> &cond, std::vector<std::unique_ptr<Statement>> &f_b) : condition(cond) {
		for (auto &x : f_b)
			for_body.emplace_back(x.release());
	}

	void execute(std::vector<VariableContainer> &var_container) override;

};

class Find : public Statement {
private:
	std::vector<Token> statement;


public:
	Find(const std::vector<Token> &tokens) : statement(tokens) {}
	void execute(std::vector<VariableContainer> &var_container) override;

};

class Print : public Statement {
private:
    std::vector<Token> expression;


public:
    Print(const std::vector<Token> &tokens) : expression(tokens) {}
    void execute(std::vector<VariableContainer> &var_container) override;

};



#endif

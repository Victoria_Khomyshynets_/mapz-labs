#include "SyntaxAnalyzer.h"

unsigned SyntaxAnalyzer::handle_if(const unsigned &if_pos, std::vector<VariableContainer> &var_container,
	std::vector<std::unique_ptr<Statement>> &statements) {
	//CHECK FOR CONDITION
	unsigned index_cond_lparent = if_pos + 1, index_cond_rparent = if_pos + 1;
	while (index_cond_rparent < tokens.size() && tokens[index_cond_rparent].get_type() != Token::Type::rparent)
		++index_cond_rparent;
	if (index_cond_lparent >= tokens.size() || tokens[index_cond_lparent].get_type() != Token::Type::lparent ||
		index_cond_rparent >= tokens.size())
		throw std::logic_error("SYNTAX ERROR: EXPECTED CONDITION AFTER IF BRANCH");

	//CHECK FOR IF BODY
	unsigned index_body_lbrace = index_cond_rparent + 1, index_body_rbrace = index_cond_rparent + 1;		//TO FIRST TOKEN AFTER LBRACE
	int brace_counter = 0;
	while (index_body_rbrace < tokens.size()) {
		if (tokens[index_body_rbrace].get_type() == Token::Type::lbrace)
			++brace_counter;
		else if (tokens[index_body_rbrace].get_type() == Token::Type::rbrace)
			--brace_counter;
		if (brace_counter == 0)
			break;
		++index_body_rbrace;
	}

	if (index_body_lbrace >= tokens.size() || tokens[index_body_lbrace].get_type() != Token::Type::lbrace
		|| index_body_rbrace >= tokens.size())
		throw std::logic_error("SYNTAX ERROR: EXPECTED CLOSING BRACE");


	//STATEMENTS LISTS
	std::vector<std::unique_ptr<Statement>> stat_if;
	std::vector<std::unique_ptr<Statement>> stat_else;

	//CREATE STATEMENTS LIST FOR IF BODY 
	SyntaxAnalyzer sa(std::vector<Token>(tokens.begin() + index_body_lbrace + 1, tokens.begin() + index_body_rbrace));
	sa.execute(var_container, stat_if);


	//CHECK FOR ELSE
	unsigned index_else = index_body_rbrace + 1;
	unsigned index_else_lbrace = index_else + 1, index_else_rbrace = index_else + 1;

	if (index_else < tokens.size() && tokens[index_else].token() == "else") {
		brace_counter = 0;
		while (index_else_rbrace < tokens.size()) {
			if (tokens[index_else_rbrace].get_type() == Token::Type::lbrace)
				++brace_counter;
			else if (tokens[index_else_rbrace].get_type() == Token::Type::rbrace)
				--brace_counter;
			if (brace_counter == 0)
				break;
			++index_else_rbrace;
		}

		if (index_else_lbrace >= tokens.size() || tokens[index_else_lbrace].get_type() != Token::Type::lbrace
			|| index_else_rbrace >= tokens.size())
			throw std::logic_error("Closing brace for else expected!");

		// CREATE STATEMENTS LIST FOR ELSE BODY
		SyntaxAnalyzer sa(std::vector<Token>(tokens.begin() + index_else_lbrace + 1, tokens.begin() + index_else_rbrace));
		sa.execute(var_container, stat_else);

		++index_else_rbrace;
	}
	else
			index_else_rbrace -= 2;


	//CREATE STATEMENT FOR GENERAL IF BRANCH
	statements.emplace_back(new IfBranch(std::vector<Token>(tokens.begin() + index_cond_lparent + 1, tokens.begin() + index_cond_rparent),
		stat_if, stat_else));
	return  index_else_rbrace;
}


template<typename T>
unsigned SyntaxAnalyzer::handle_loop(const unsigned & while_pos, std::vector<VariableContainer>& var_container, std::vector<std::unique_ptr<Statement>>& statements)
{
	if (!std::is_same<T, WhileLoop>::value && !std::is_same<T, ForLoop>::value)
		throw std::logic_error("Wrong loop operation!");

	//CHECK FOR CONDITION
	unsigned index_cond_lparent = while_pos + 1, index_cond_rparent = while_pos + 1;
	while (index_cond_rparent < tokens.size() && tokens[index_cond_rparent].get_type() != Token::Type::rparent)
		++index_cond_rparent;
	if (index_cond_lparent >= tokens.size() || tokens[index_cond_lparent].get_type() != Token::Type::lparent ||
		index_cond_rparent >= tokens.size())
		throw std::logic_error("SYNTAX ERROR: EXPECTED CONDITION AFTER IF BRANCH");

	//CHECK FOR WHILE BODY
	unsigned index_body_lbrace = index_cond_rparent + 1, index_body_rbrace = index_cond_rparent + 1;		//TO FIRST TOKEN AFTER LBRACE
	int brace_counter = 0;
	while (index_body_rbrace < tokens.size()) {
		if (tokens[index_body_rbrace].get_type() == Token::Type::lbrace)
			++brace_counter;
		else if (tokens[index_body_rbrace].get_type() == Token::Type::rbrace)
			--brace_counter;
		if (brace_counter == 0)
			break;
		++index_body_rbrace;
	}

	if (index_body_lbrace >= tokens.size() || tokens[index_body_lbrace].get_type() != Token::Type::lbrace
		|| index_body_rbrace >= tokens.size())
		throw std::logic_error("SYNTAX ERROR: EXPECTED CLOSING BRACE");

	//STATEMENTS LISTS
	std::vector<std::unique_ptr<Statement>> stat_while;

	//CREATE STATEMENTS LIST FOR WHILE BODY 
	SyntaxAnalyzer sa(std::vector<Token>(tokens.begin() + index_body_lbrace + 1, tokens.begin() + index_body_rbrace));
	sa.execute(var_container, stat_while);


	//CREATE STATEMENT FOR GENERAL IF BRANCH
	statements.emplace_back(new T(std::vector<Token>(tokens.begin() + index_cond_lparent + 1, tokens.begin() + index_cond_rparent), stat_while));


	return index_body_rbrace;
}


void SyntaxAnalyzer::execute(std::vector<VariableContainer> &var_container, std::vector<std::unique_ptr<Statement>> &statements)
{
	if (!check_braces()) throw std::logic_error("Check braces parity!");
	if (!check_paretheses()) throw std::logic_error("Check parentheses parity!");


	for (unsigned i = 0; i < tokens.size();)
	{
		if (tokens[i].get_type() == Token::Type::varname)
		{
			unsigned j = i + 1;
			while (j < tokens.size() && tokens[j].get_type() != Token::Type::semicolon)
				++j;
			if (j == tokens.size())
				throw std::logic_error("Expected semicolon in assignment operation!");

			statements.emplace_back(new Assign(std::vector<Token>(tokens.begin() + i, tokens.begin() + j + 1)));

			i = j;
		}

		else if(tokens[i].token() == "if")
		{
			i = handle_if(i, var_container, statements);
		}
		else if (tokens[i].token() == "while")
		{
			i = handle_loop<WhileLoop>(i, var_container, statements);
		}
		else if (tokens[i].token() == "for")
		{
			i = handle_loop<ForLoop>(i, var_container, statements);
		}
		else if (tokens[i].token() == "find")
		{
			unsigned j = i + 1;
			while (j < tokens.size() && tokens[j].get_type() != Token::Type::semicolon)
				++j;
			if (j == tokens.size())
				throw std::logic_error("Expected semicolon in search operation!");
			statements.emplace_back(new Find(std::vector<Token>(tokens.begin() + i, tokens.begin() + j + 1)));
			i = j;
		}
        else if (tokens[i].token() == "print")
        {
            unsigned j = i + 1;
            while (j < tokens.size() && tokens[j].get_type() != Token::Type::semicolon)
                ++j;
            if (j == tokens.size())
                throw std::logic_error("Expected semicolon in print operation!");
            statements.emplace_back(new Print(std::vector<Token>(tokens.begin() + i, tokens.begin() + j + 1)));
            i = j;
        }
		else
			++i;
	}
}



bool SyntaxAnalyzer::check_braces()
{
	int counter{ 0 };
	for (const auto & x : tokens)
	{
		if (x.token().compare("{") == 0)
			++counter;
		else if (x.token().compare("}") == 0)
			--counter;
	}
	return counter == 0;
}

bool SyntaxAnalyzer::check_paretheses()
{
	int counter{ 0 };
	for (const auto & x : tokens)
	{
		if (x.token().compare("(") == 0)
			++counter;
		else if (x.token().compare(")") == 0)
			--counter;
	}
	return counter == 0;
}

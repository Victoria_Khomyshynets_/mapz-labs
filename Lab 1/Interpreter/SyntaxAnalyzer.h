#ifndef SYNTAX_ANALYZER_H
#define SYNTAX_ANALYZER_H

#include <iostream>
#include "Token.h"
#include "Statement.h"
#include "VarContainer.h"
#include <vector>
#include <list>
#include <memory>



class SyntaxAnalyzer {
private:
	std::vector<Token> tokens;

	bool check_braces();
	bool check_paretheses();
	unsigned handle_if(const unsigned &if_pos, std::vector<VariableContainer> &var_container, std::vector<std::unique_ptr<Statement>> &statements);
	template<typename T> unsigned handle_loop(const unsigned &while_pos, std::vector<VariableContainer> &var_container, std::vector<std::unique_ptr<Statement>> &statements);


public:

    SyntaxAnalyzer(const std::vector<Token> &tok) : tokens(tok) {}
	void execute(std::vector<VariableContainer> &var_container, std::vector<std::unique_ptr<Statement>> &statements);

};





#endif

#ifndef TOKEN_H
#define TOKEN_H

#include <iostream>



class Token {
public:
	enum class Type {
		lparent, rparent, lbrace, rbrace, keyword, colon,
		vartype, varname, constant, oper, logical_oper, semicolon
	};

private:
	std::string data;
	Type type;

public:
	Token(std::string str, Type t) : data(str), type(t) {}
	std::string token() const { return data; }
	Type get_type() const { return type; }

};






#endif

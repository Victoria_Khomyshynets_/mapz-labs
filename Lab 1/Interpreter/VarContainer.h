#ifndef VAR_CONTAINER_H
#define VAR_CONTAINER_H

#include <iostream>
#include <map>
#include <string>

class VariableContainer {
private:
	std::map<std::string, std::string> variable;


public:
	std::string &operator[](const std::string &str) { return variable[str]; }
	bool erase(const std::string &str) { return variable.erase(str); }
	bool exist(const std::string &key) { return variable.find(key) != variable.end(); }

};





#endif
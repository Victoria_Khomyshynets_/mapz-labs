#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <fstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //interp = new Interpreter();
}

MainWindow::~MainWindow()
{
    delete ui;
    //delete interp;
    //interp = nullptr;
}

void MainWindow::on_pushButtonRun_clicked()
{
    interp = new Interpreter();
    try {
        std::string raw_code = ui->textEditCode->toPlainText().toUtf8().constData();
        interp->set_code(raw_code);
        interp->execute();

        std::ifstream file("D:\\Interpreter\\output.txt", std::ios::in);
        if(!file)
            throw std::logic_error("cannot open file!");
        std::string output((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
        ui->textEditOutput->setText(output.c_str());
        file.close();

        std::ofstream ofs;
        ofs.open("D:\\Interpreter\\output.txt", std::ofstream::out | std::ofstream::trunc);
        ofs.close();

    } catch (std::exception &e) {
        ui->textEditOutput->append(e.what());
    }
    delete interp;
    interp = nullptr;
}

#ifndef FLEXERBUILDER_H
#define FLEXERBUILDER_H
#include "FLEXER.h"

class FLEXERbuilder
{
public:
    virtual Muscle* getMuscle() = 0;
   
};


#endif // FLEXERBUILDER_H

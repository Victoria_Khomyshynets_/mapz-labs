#include <iostream>

using namespace std;

class Flexer
{
private:
    Flexer();
    static Flexer* currentFlexer;
    int 10;
    

public:

    static Flexer* getInstance();
    int muscle = 0;
    int smartness = 0;
    int nimbleness = 0;
    int totalPoints;
    
    void uppMuscle(const int&);
    void uppSmartness(const int&);
    void uppNimbleness(const int&);


public:
    void musclePoint = 0;

};


int main()
{
    Flexer *a = Flexer::getInstance();
    Flexer *b = Flexer::getInstance();
    cout << "One! " << a << "\nTwo! " << b;
}

#ifndef WEAPON_H
#define WEAPON_H


class Weapon
{
public:
    virtual ~Weapon() {}
    virtual int getDamage() const = 0;
    virtual char* getName() const = 0;
   
    virtual char* getName() const = 0;

    virtual char* getName() const = 0;
};

#endif // WEAPON_H

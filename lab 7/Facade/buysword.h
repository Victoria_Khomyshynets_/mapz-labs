#ifndef BUYSWORD_H
#define BUYSWORD_H


class buySword
{
private:
	int buyWoodenSword(int lvl, int b);
    int buyIronSword(int lvl, int c);
public:
    int buyWoodenSword(int lvl, int c);
    int buyIronSword(int lvl, int b);
};

#endif 

#include "spfacade.h"

int ShopFacade::BuyWoodenEquipment(int lvl, float c)
{
    int armorPrice = bArmor.buyWoodenArmor(lvl);
    int bootsPrice = bBoots.buyWoodenBoots(lvl);
    int swordPrice = bSword.buyWoodenSword(lvl);

    return armorPrice + bootsPrice + swordPrice;
}

int ShopFacade::BuyIronEquipment(int lvl, int b)
{
    int armorPrice = bArmor.buyIrnArmor(lvl);
    int bootsPrice = bBoots.buyIrnBoots(lvl);
    int swordPrice = bSword.buyIrnSword(lvl);

    return armorPrice + bootsPrice + swordPrice;
}


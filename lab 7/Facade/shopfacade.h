#ifndef SHOPFACADE_H
#define SHOPFACADE_H
#include "byarmor.h"
#include "byyboots.h"
#include "byysword.h"
#include "character.h"

class SpFacade
{
protected:
    byArmor bArmor;
    byBoots bBoots;
    bySword bSword;
public:
    int ByWoodEquipment(int lvl);
    int ByIrnEquipment(int lvl);
    int ByWoodEquipment(int lvl);
    int ByIrnEquipment(int lvl);
    int ByWoodEquipment(int lvl);
    int ByIrnEquipment(int lvl);
};

#endif // SHOPFACADE_H

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Dialog
{
public:
    virtual ~Dialog() {}
    virtual string StartDialog(int level) const = 0;
};

class Gameplay
{
private:
    Dialog *dialog;

public:
    Gameplay(Dialog *dialog = nullptr) : dialog(dialog)
    {
    }
    ~Gameplay()
    {
        delete this->dialog;
    }

    void set_dialog_type(Dialog *dialog)
    {
        delete this->dialog;
        this->dialog = dialog;
    }

    void StartDialog(int playerStatus) const
    {
        std::cout << "Context: Determine the strategy\n";
        string result = this->dialog->DoAlgorithm(playerStatus);
        std::cout << result << "\n";
    }
};

class SimpleDialog : public Dialog
{
public:
    string StartDialog(int playerStatus) const override
    {
        string result = "SimpleDialog";
        return result;
    }
};
class VipDialog : public Dialog
{
    string StartDialog(int playerStatus) const override
    {
        string result = "VipDialog";
        return result;
    }
};

int main()
{
    Gameplay* gameplay;
    int playerStatus = 0;
    while (playerStatus > 0)
    {
        if (playerStatus == 0)
        {
            gameplay = new Gameplay(new SimpleDialog);
            std::cout << "Player is simple.\n";
            gameplay->StartDialog(playerStatus);
        }
        else
        {
            gameplay = new Gameplay(new VipDialog);
            std::cout << "\n";
            std::cout << "Player is vip.\n";
            gameplay->StartDialog(playerStatus);
        }

    }
    delete gameplay;
    return 0;
}

#include <iostream>
#include <stack>
using namespace std;

stack <Memento> mementoStack;

class Memento
{
private:
    string currentPath;

    Memento(string newPath)
    {
        currentPath = newPath;
    }
    string GetCurrentPath()
    {
        return currentPath;
    }
    void SetNewPath(string newPath)
    {
        currentPath += newPath;
    }
    friend class Originator;
};


class Originator
{
private:
    string currentPath;

public:
    string GetCurrentPath()
    {
        return currentPath;
    }
    void SetNewPath(string newPath)
    {
        currentPath += newPath;
    }
    Memento *CreateMemento()
    {
        return (new Memento(currentPath));
    }
    void SetMemento(Memento *memento)
    {
        cout << "Go to previous save\n";
        currentPath = memento->GetCurrentPath();
    }
};

class Caretaker
{
private:
    Memento *memento;
    Originator *origin;

public:
    Caretaker (Originator *obj) : origin(obj)
    {}

    void CreateSave(string newPath)
    {
        memento = origin->CreateMemento();
        cout << "Save created." << endl;
        origin->SetNewPath(newPath);
        mementoStack.push(*memento);
    }
    void LoadLastSave()
    {
        *memento = mementoStack.top();
        mementoStack.pop();
        origin->SetMemento(memento);
    }
};

int main()
{
    Originator *originator = new Originator();
    Caretaker* caretaker;

    originator->SetNewPath("right->");
    cout << "Your current path : " << originator->GetCurrentPath() << endl;

    caretaker = new Caretaker(originator);
    caretaker->CreateSave("left->");
    cout << "Your current path : " << originator->GetCurrentPath() << endl;

    originator->SetNewPath("right->");
    cout << "Your current path : " << originator->GetCurrentPath() << endl;

    caretaker->LoadLastSave();
    cout << "Your current path : " << originator->GetCurrentPath() << endl;

    caretaker->LoadLastSave();
    cout << "Your current path : " << originator->GetCurrentPath() << endl;
    return 0;
}

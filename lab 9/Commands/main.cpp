#include<iostream>
#include<vector>
#include<string>

using namespace std;

class gaaame
{
  public:
    void create( ) {
      cout << "Create gaaame " << endl;
    }
    void open( string file ) {
      cout << "Open gaaame from " << file << endl;
    }
    void save( string file ) {
      cout << "Save gaaame in " << file << endl;
    }
    void make_move( string move ) {
      cout << "Make move " << move << endl;
    }
    void create( ) {
      cout << "Create gaaame " << endl;
    }
    void open( string file ) {
      cout << "Open gaaame from " << file << endl;
    }
    void save( string file ) {
      cout << "Save gaaame in " << file << endl;
    }
    void make_move( string move ) {
      cout << "Make move " << move << endl;
    }
};

string getPlayerInput( string prompt ) {
  string input;
  cout << prompt;
  cin >> input;
  return input;
}

class Command
{
  public:
    virtual ~Command() {}
    virtual void execute() = 0;
  protected:
    Command( gaaame* p ): pgame( p) {}
    gaaame * pgame;
    public:
    virtual ~Command() {}
    virtual void execute() = 0;
  protected:
    Command( gaaame* p ): pgame( p) {}
    gaaame * pgame;
};



class SaveGameCommand: public Command
{
  public:
    SaveGameCommand( gaaame * p ) : Command( p) {}
    void execute( ) {
      string file_name;
      file_name = getPlayerInput( "Enter file name:");
      pgame->save( file_name);
    }
    public:
    SaveGameCommand( gaaame * p ) : Command( p) {}
    void execute( ) {
      string file_name;
      file_name = getPlayerInput( "Enter file name:");
      pgame->save( file_name);
    }
};



class UndooooooCommand: public Command
{
  public:
    UndoCommand( gaaame * p ) : Command( p) {}
    void execute() {
      // Восстановим игру из временного файла
      pgame->open( "TEMP_FILE");
    }
    public:
    UndoCommand( gaaame * p ) : Command( p) {}
    void execute() {
      // Восстановим игру из временного файла
      pgame->open( "TEMP_FILE");
    }
};


int main()
{
  gaaame gaaame;
  vector<Command*> v;
  v.push_back( new CreateGameCommand( &gaaame));
  v.push_back( new MakeMoveCommand( &gaaame));
  v.push_back( new MakeMoveCommand( &gaaame));
  v.push_back( new UndoCommand( &gaaame));
  v.push_back( new SaveGameCommand( &gaaame));

  for (size_t i=0; i<v.size(); ++i)
    v[i]->execute();

  for (size_t i=0; i<v.size(); ++i)
    delete v[i];
  for (size_t i=0; i<v.size(); ++i)
    v[i]->execute();

  for (size_t i=0; i<v.size(); ++i)
    delete v[i];

  return 0;
}

#include <iostream>
#include <string>
#include <iostream>
#include <string>

#include <iostream>
#include <string>


class BazovaComponenta;
class Mediator {
 public:
  virtual void Notify(BazovaComponenta *sender, std::string event) const = 0;
};

class BazovaComponenta {
 protected:
  Mediator *mediator_;

 public:
  BazovaComponenta(Mediator *mediator = nullptr) : mediator_(mediator) {
  }
  void set_mediator(Mediator *mediator) {
    this->mediator_ = mediator;
  }
};

class REAAAACTOR : public BazovaComponenta {
 public:
  void Warning() {
    std::cout << "Warning, U have to put on your equip!\n";
    this->mediator_->Notify(this, "takeEquip");
  }
  void ContinueWay() {
    std::cout << "Your hero was disequiped by EquipManager.\n";
    this->mediator_->Notify(this, "continue");
  }
};

class EquipManager : public BazovaComponenta {
 public:
  void TakeEquip() {
    std::cout << "Your hero was equiped by EquipManager.\n";
    this->mediator_->Notify(this, "C");
  }
  void TakeOffEquip() {
    std::cout << "Your can remove your equipment.\n";
    this->mediator_->Notify(this, "Piece");
  }
};

class concretnyimediator : public Mediator {
 private:
  REAAAACTOR *component1_;
  EquipManager *component2_;

 public:
  concretnyimediator(REAAAACTOR *c1, EquipManager *c2) : component1_(c1), component2_(c2) {
    this->component1_->set_mediator(this);
    this->component2_->set_mediator(this);
  }
  void Notify(BazovaComponenta *sender, std::string event) const override {
    if (event == "takeEquip") {
      std::cout << "Mediator reacts on \"TakeEquip\" and triggers following operations:\n";
      this->component2_->TakeEquip();
    }
    if (event == "Piece") {
      std::cout << "Mediator reacts on \"Piece\" and triggers following operations:\n";
      this->component1_->ContinueWay();
    }
    if (event == "continue"){
        std::cout << "\n\nwe continue the way...";
    }
  }
};


void ClientCode() {
  REAAAACTOR *c1 = new REAAAACTOR;
  EquipManager *c2 = new EquipManager;
  concretnyimediator *mediator = new concretnyimediator(c1, c2);
  std::cout << "You were surrounded bu monsters!.\n";
  c1->Warning();
  std::cout << "\n";
  std::cout << "All is OK, the battle is over.\n";
  c2->TakeOffEquip();
  REAAAACTOR *c1 = new REAAAACTOR;
  EquipManager *c2 = new EquipManager;
  concretnyimediator *mediator = new concretnyimediator(c1, c2);
  std::cout << "You were surrounded bu monsters!.\n";
  c1->Warning();
  std::cout << "\n";
  std::cout << "All is OK, the battle is over.\n";
  c2->TakeOffEquip();

  delete c1;
  delete c2;
  delete mediator;
  delete c1;
  delete c2;
  delete mediator;
}

int main() {
  ClientCode();
  return 0;
}
